<nav class="tabs">
    <div class="container">
        <ul>
            <router-link tag="li" to="/" exact>
                <a>Home</a>
            </router-link>
            <router-link to="/about">
                <a>About</a>
            </router-link>
            <router-link to="/contact">
                <a>Contact</a>
            </router-link>
        </ul>
    </div>
</nav>
